data "google_container_cluster" "gke_cluster" {
  depends_on = [
    google_container_node_pool.primary_nodes,
    google_container_cluster.primary
  ]
  name = google_container_cluster.primary.name
  location = google_container_cluster.primary.location
}

output "ca_certificate" {
  value = base64decode(data.google_container_cluster.gke_cluster.master_auth.0.cluster_ca_certificate)
  # value = google_container_cluster.primary.master_auth.0.cluster_ca_certificate
}
# output "client_key" {
#   depends_on = [
#     google_container_node_pool.primary_nodes,
#   ]
#   value = google_container_cluster.primary.master_auth.0.client_key
# }
# output "client_certificate" {
#   depends_on = [
#     google_container_node_pool.primary_nodes,
#   ]
#   value = google_container_cluster.primary.master_auth.0.client_certificate
# }
output "cluster_endpoint" {
  value = data.google_container_cluster.gke_cluster.endpoint
  # value = data.google_container_cluster.gke_cluster.endpoint
}
output "cluster_name" {
  depends_on = [
    google_container_node_pool.primary_nodes,
  ]
  # value = google_container_cluster.primary.name
  value = data.google_container_cluster.gke_cluster.name
}
output "node_version" {
  value = data.google_container_engine_versions.k8sversion.latest_node_version
}
output "kubeconfig" {
    value = templatefile("${path.module}/kubeconfig.yaml", {
        cluster_name = google_container_cluster.primary.name,
        endpoint =  data.google_container_cluster.gke_cluster.endpoint,
        user_name = "gke_${var.gcp_project}_${var.gcp_zone}_${google_container_cluster.primary.name}",
        cluster_ca = data.google_container_cluster.gke_cluster.master_auth.0.cluster_ca_certificate,
        client_cert = data.google_container_cluster.gke_cluster.master_auth.0.client_certificate,
        client_cert_key = data.google_container_cluster.gke_cluster.master_auth.0.client_key,
        user_password = data.google_container_cluster.gke_cluster.master_auth.0.password
        access_token = data.google_client_config.current.access_token
    })
    sensitive = true
}