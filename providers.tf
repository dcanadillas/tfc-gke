terraform {
  required_version = ">= 0.14"
  # backend "remote" {}
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.63.0"
    }
    # vault = {
    #   source = "hashicorp/vault"
    #   version = "2.19.0"
    # }
  }
}

provider "google" {
  project     = var.gcp_project
  region      = var.gcp_region
}

# provider vault {
#   namespace = var.vault_ns
#   address = var.vault_addr
# }